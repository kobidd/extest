// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyClWyaZoIIGupmu_vve3Y2q2bTtWClFTH4",
    authDomain: "extest-6d456.firebaseapp.com",
    databaseURL: "https://extest-6d456.firebaseio.com",
    projectId: "extest-6d456",
    storageBucket: "extest-6d456.appspot.com",
    messagingSenderId: "30475766641",
    appId: "1:30475766641:web:e2fd8b64263aaa3be304d0",
    measurementId: "G-LSDYC4HKL2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
