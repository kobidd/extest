import { DocumentService } from './../document.service';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';


@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {
  
  category:string = "Loading...."
  catrgorytypes:object[] = [{name:'business'},{name:'entertainment'} ,{name:'politics'},{name:'sport'},{name:'tech'}];
  body:string
  authCategory:string
  constructor(public classifyservice:ClassifyService,
              private documentservice:DocumentService) { }

  ngOnInit() {
    this.classifyservice.classify().subscribe(
      res =>{
        this.category = this.classifyservice.categories[res];
        this.body = this.classifyservice.doc;
      }
    )
  }
  onSubmitOrg(){
    this.documentservice.addDoc(this.body,this.category)
  }
  onSubmitAut(){
    this.documentservice.addDoc(this.body,this.authCategory)
  }

}
