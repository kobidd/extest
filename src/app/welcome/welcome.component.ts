import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  path:string
  title:string
  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
    this.path = this.route.snapshot.params['path'];
    if(this.path=="logIn"){
      this.title = "Log In Success"
    }
    else{
      this.title = "Sign Up Success"
    }

  }

}
