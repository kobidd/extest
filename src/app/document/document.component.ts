import { Observable } from 'rxjs';
import { DocumentService } from './../document.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
articles$:Observable<any>
  constructor(private documentservice:DocumentService) { }

  ngOnInit() {
   this.articles$= this.documentservice.getDocs()
  }

}
