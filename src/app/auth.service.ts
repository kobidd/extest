import { Observable, throwError } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
user:Observable<User|null>
errora:any
errorfinal:any

  constructor(private afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
   }

   signup(email:string,password:string){
     this.afAuth.auth.createUserWithEmailAndPassword(email,password)
     .then(user=> this.router.navigate(['welcome/signUp']),catchError(this.handleError))
     .catch(error=>{
      console.log(error.message)
      this.errora = error.message})
   }
   logOut(){
     this.afAuth.auth.signOut()
     .then(user=> this.router.navigate(['login']))
   }
   logIn(email:string,password:string){
     this.afAuth.auth.signInWithEmailAndPassword(email,password)
     .then(user=> this.router.navigate(['welcome/logIn']))
     .catch(error=>{
       console.log(error.message)
       this.errora = error.message})
      //alert(error.message);
     /*if(catchError(this.handleError)){
      this.errorfinal = this.errora
      console.log(this.errorfinal)
     }*/
   }

   public handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
  }

}
