import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private db:AngularFirestore,
              private router:Router) { }

  addDoc(body:string,category:string){
    const doc = {body:body,category:category}
    this.db.collection('documents').add(doc)
    this.router.navigate(['document'])
  }

  getDocs():Observable<any>{
    return this.db.collection('documents').valueChanges();
  }
}
