import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { error } from 'util';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
email:string
password:string
errorMessage:string
  constructor(public authservice:AuthService) { }

  ngOnInit() {
  }
onSubmit(){
this.authservice.signup(this.email,this.password);

}
}
